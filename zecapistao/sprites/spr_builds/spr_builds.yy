{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 255,
  "bbox_top": 1,
  "bbox_bottom": 191,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 256,
  "height": 192,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"dd786098-153c-417d-8e55-fb65e72173f7","path":"sprites/spr_builds/spr_builds.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd786098-153c-417d-8e55-fb65e72173f7","path":"sprites/spr_builds/spr_builds.yy",},"LayerId":{"name":"aa4eb807-c894-4f6e-b366-f25e9b105cd7","path":"sprites/spr_builds/spr_builds.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_builds","path":"sprites/spr_builds/spr_builds.yy",},"resourceVersion":"1.0","name":"dd786098-153c-417d-8e55-fb65e72173f7","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_builds","path":"sprites/spr_builds/spr_builds.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"149d7d52-2081-4dd8-a25c-3784488d97af","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd786098-153c-417d-8e55-fb65e72173f7","path":"sprites/spr_builds/spr_builds.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_builds","path":"sprites/spr_builds/spr_builds.yy",},
    "resourceVersion": "1.3",
    "name": "spr_builds",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"aa4eb807-c894-4f6e-b366-f25e9b105cd7","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "builds",
    "path": "folders/Sprites/background/builds.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_builds",
  "tags": [],
  "resourceType": "GMSprite",
}