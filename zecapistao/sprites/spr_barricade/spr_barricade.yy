{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 37,
  "bbox_top": 0,
  "bbox_bottom": 30,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 38,
  "height": 31,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"701a7508-a5d8-4bea-a1e4-33a97adb3a7b","path":"sprites/spr_barricade/spr_barricade.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"701a7508-a5d8-4bea-a1e4-33a97adb3a7b","path":"sprites/spr_barricade/spr_barricade.yy",},"LayerId":{"name":"5c97c195-a9ec-4ea0-8782-52c71fe15e0e","path":"sprites/spr_barricade/spr_barricade.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_barricade","path":"sprites/spr_barricade/spr_barricade.yy",},"resourceVersion":"1.0","name":"701a7508-a5d8-4bea-a1e4-33a97adb3a7b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_barricade","path":"sprites/spr_barricade/spr_barricade.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5cbaf86f-de5f-47a4-90d9-b2f6456cb2d0","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"701a7508-a5d8-4bea-a1e4-33a97adb3a7b","path":"sprites/spr_barricade/spr_barricade.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 19,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_barricade","path":"sprites/spr_barricade/spr_barricade.yy",},
    "resourceVersion": "1.3",
    "name": "spr_barricade",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5c97c195-a9ec-4ea0-8782-52c71fe15e0e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "builds",
    "path": "folders/Sprites/builds.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_barricade",
  "tags": [],
  "resourceType": "GMSprite",
}