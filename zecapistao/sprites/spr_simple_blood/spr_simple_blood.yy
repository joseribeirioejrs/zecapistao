{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 46,
  "bbox_top": 0,
  "bbox_bottom": 45,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 47,
  "height": 46,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"8c4c7d03-48c1-48df-a0f5-a470c13187ab","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8c4c7d03-48c1-48df-a0f5-a470c13187ab","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"8c4c7d03-48c1-48df-a0f5-a470c13187ab","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"546f22c2-02f6-41a8-86e4-4c796e8656db","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"546f22c2-02f6-41a8-86e4-4c796e8656db","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"546f22c2-02f6-41a8-86e4-4c796e8656db","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"81a7396f-bc7b-4d67-8036-4d5158c3085b","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"81a7396f-bc7b-4d67-8036-4d5158c3085b","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"81a7396f-bc7b-4d67-8036-4d5158c3085b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"89c333e2-479a-4042-a250-4a5716d84b08","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"89c333e2-479a-4042-a250-4a5716d84b08","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"89c333e2-479a-4042-a250-4a5716d84b08","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"18c3816b-693f-407b-a23b-5b86744212ca","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"18c3816b-693f-407b-a23b-5b86744212ca","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"18c3816b-693f-407b-a23b-5b86744212ca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f2924f77-ec7f-42d3-a922-4a83dd1e6360","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f2924f77-ec7f-42d3-a922-4a83dd1e6360","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"f2924f77-ec7f-42d3-a922-4a83dd1e6360","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c74de07a-4fb5-47cd-9d70-e9ae8d194424","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c74de07a-4fb5-47cd-9d70-e9ae8d194424","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"c74de07a-4fb5-47cd-9d70-e9ae8d194424","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6aa5f79d-49c2-4352-9732-b2c935f56d0b","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6aa5f79d-49c2-4352-9732-b2c935f56d0b","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"6aa5f79d-49c2-4352-9732-b2c935f56d0b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c0e891bb-620f-482a-9245-d9e37871e364","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c0e891bb-620f-482a-9245-d9e37871e364","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"c0e891bb-620f-482a-9245-d9e37871e364","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"de6524bd-5b23-47c6-b661-5599bb6c9980","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"de6524bd-5b23-47c6-b661-5599bb6c9980","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"de6524bd-5b23-47c6-b661-5599bb6c9980","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"adb0e299-36ee-4686-9891-fb584c51202e","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"adb0e299-36ee-4686-9891-fb584c51202e","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"adb0e299-36ee-4686-9891-fb584c51202e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"06729bea-b998-4d1e-b926-e36d74b91e0c","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"06729bea-b998-4d1e-b926-e36d74b91e0c","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"06729bea-b998-4d1e-b926-e36d74b91e0c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ad032a26-5b18-4834-8762-e2385b08453c","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ad032a26-5b18-4834-8762-e2385b08453c","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"ad032a26-5b18-4834-8762-e2385b08453c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fb90dae1-5e29-47b8-b85c-e67808c6486a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fb90dae1-5e29-47b8-b85c-e67808c6486a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"fb90dae1-5e29-47b8-b85c-e67808c6486a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"960e6e0e-f82b-4c2d-bcf1-aea36b3fded8","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"960e6e0e-f82b-4c2d-bcf1-aea36b3fded8","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"960e6e0e-f82b-4c2d-bcf1-aea36b3fded8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"09c577ae-1638-4190-935c-ccaa4abc14b8","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"09c577ae-1638-4190-935c-ccaa4abc14b8","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"09c577ae-1638-4190-935c-ccaa4abc14b8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"613d1fc7-272b-4d13-8281-7d8cc33870fb","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"613d1fc7-272b-4d13-8281-7d8cc33870fb","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"613d1fc7-272b-4d13-8281-7d8cc33870fb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"980add76-e6c5-4603-bb79-07d358d08cf3","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"980add76-e6c5-4603-bb79-07d358d08cf3","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"980add76-e6c5-4603-bb79-07d358d08cf3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dd6b03d2-c66d-4b78-9f58-7a138eb03fb7","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd6b03d2-c66d-4b78-9f58-7a138eb03fb7","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"dd6b03d2-c66d-4b78-9f58-7a138eb03fb7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5dab1544-2fb0-4bf6-8ad1-c26822524504","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5dab1544-2fb0-4bf6-8ad1-c26822524504","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"5dab1544-2fb0-4bf6-8ad1-c26822524504","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fb853f67-8989-4d64-9976-8e521f199ee5","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fb853f67-8989-4d64-9976-8e521f199ee5","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"fb853f67-8989-4d64-9976-8e521f199ee5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"05db91da-7241-487f-b90a-f8043f2fa901","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"05db91da-7241-487f-b90a-f8043f2fa901","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"05db91da-7241-487f-b90a-f8043f2fa901","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4d449970-04d8-40d2-a271-389d756eef48","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4d449970-04d8-40d2-a271-389d756eef48","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"4d449970-04d8-40d2-a271-389d756eef48","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"be37fa62-8f79-4852-9b13-c78340643c48","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"be37fa62-8f79-4852-9b13-c78340643c48","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"be37fa62-8f79-4852-9b13-c78340643c48","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4a00db65-be4b-43c6-83e8-8204da826777","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4a00db65-be4b-43c6-83e8-8204da826777","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"4a00db65-be4b-43c6-83e8-8204da826777","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2b299570-7b34-4f9c-8174-bf549552af5a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2b299570-7b34-4f9c-8174-bf549552af5a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"2b299570-7b34-4f9c-8174-bf549552af5a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"109e51e3-fccd-493a-9e6f-bf6205fb7b65","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"109e51e3-fccd-493a-9e6f-bf6205fb7b65","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"109e51e3-fccd-493a-9e6f-bf6205fb7b65","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e3b141d4-f9a7-4cc0-87f6-baa80726a9c1","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e3b141d4-f9a7-4cc0-87f6-baa80726a9c1","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"e3b141d4-f9a7-4cc0-87f6-baa80726a9c1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"49c61890-1810-432a-af59-52b321ce4768","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"49c61890-1810-432a-af59-52b321ce4768","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","name":"49c61890-1810-432a-af59-52b321ce4768","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 45.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 29.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"f6ca4c30-58c6-47e9-89a5-0846b3f9ef5e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8c4c7d03-48c1-48df-a0f5-a470c13187ab","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7197718f-f564-42d6-aa99-2ec7536c98d6","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"546f22c2-02f6-41a8-86e4-4c796e8656db","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"02116c94-fafc-4146-a296-77c245ac02d0","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"81a7396f-bc7b-4d67-8036-4d5158c3085b","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7102f61d-968a-4b3a-980b-846d5f20b7fd","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"89c333e2-479a-4042-a250-4a5716d84b08","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8ea2d4e2-f90b-4f3e-8015-650b9e57ca00","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"18c3816b-693f-407b-a23b-5b86744212ca","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4d6f9583-5fd7-434b-8adb-f8f99a2700a2","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f2924f77-ec7f-42d3-a922-4a83dd1e6360","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"285c2ca9-88e1-444b-8f75-9f23c74fe902","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c74de07a-4fb5-47cd-9d70-e9ae8d194424","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a0aee6a8-bcdb-4fb3-a124-b8c12b248a43","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6aa5f79d-49c2-4352-9732-b2c935f56d0b","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"89b9716b-a552-4c4a-a6f7-e738b073136b","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c0e891bb-620f-482a-9245-d9e37871e364","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"02bbb1aa-6261-4ad8-889b-574f5a473c9b","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"de6524bd-5b23-47c6-b661-5599bb6c9980","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"47219fa1-a46b-42e2-8c0e-70d762bbcbdd","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"adb0e299-36ee-4686-9891-fb584c51202e","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0a9619bc-1f4e-4f9d-903c-3ae84efacd0d","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"06729bea-b998-4d1e-b926-e36d74b91e0c","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a72ffba1-5c27-4a33-b8a7-ce5565047970","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ad032a26-5b18-4834-8762-e2385b08453c","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"38e7291f-42d9-4156-a53b-2141ec942ad2","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fb90dae1-5e29-47b8-b85c-e67808c6486a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0a373b8b-29c9-422e-95fd-a53b720c7a98","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"960e6e0e-f82b-4c2d-bcf1-aea36b3fded8","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"616be07c-abc3-4cf3-9931-c04dff9a12e1","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"09c577ae-1638-4190-935c-ccaa4abc14b8","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"015d43aa-ad10-4c93-8626-a3c3085d25ef","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"613d1fc7-272b-4d13-8281-7d8cc33870fb","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0f9ff0d1-1749-46ce-b8db-fec5189e87d3","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"980add76-e6c5-4603-bb79-07d358d08cf3","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4ff4ceaa-e20a-4f8f-8a9c-7b338c0a9b1f","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd6b03d2-c66d-4b78-9f58-7a138eb03fb7","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8ed9c226-794f-4bad-85f4-206e678aa95a","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5dab1544-2fb0-4bf6-8ad1-c26822524504","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4cb09ea5-1550-447f-ac46-c6180bb81e7b","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fb853f67-8989-4d64-9976-8e521f199ee5","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"63b86437-2115-469c-98b6-56394838ce37","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"05db91da-7241-487f-b90a-f8043f2fa901","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a7725333-0959-450b-8d0e-7ad538321dc5","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4d449970-04d8-40d2-a271-389d756eef48","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00c30536-9884-4a6b-85ff-83738e502075","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"be37fa62-8f79-4852-9b13-c78340643c48","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9bed06da-8150-4a49-8f05-9d8faf5f8446","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4a00db65-be4b-43c6-83e8-8204da826777","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"05280f9f-5474-48ad-a2fb-9077020ed233","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2b299570-7b34-4f9c-8174-bf549552af5a","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"98a46985-89c4-4b2b-ad7f-f8e5cc9633b1","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"109e51e3-fccd-493a-9e6f-bf6205fb7b65","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"49bc5c22-5de3-4bf5-97da-0bdea07e930c","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e3b141d4-f9a7-4cc0-87f6-baa80726a9c1","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ab968d87-cbb0-47b3-8973-8e6f6269525b","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49c61890-1810-432a-af59-52b321ce4768","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 23,
    "yorigin": 23,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_simple_blood","path":"sprites/spr_simple_blood/spr_simple_blood.yy",},
    "resourceVersion": "1.3",
    "name": "spr_simple_blood",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "blood",
    "path": "folders/Sprites/blood.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_simple_blood",
  "tags": [],
  "resourceType": "GMSprite",
}