{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 46,
  "bbox_top": 0,
  "bbox_bottom": 45,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 47,
  "height": 46,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"8c4c7d03-48c1-48df-a0f5-a470c13187ab","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8c4c7d03-48c1-48df-a0f5-a470c13187ab","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"8c4c7d03-48c1-48df-a0f5-a470c13187ab","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"546f22c2-02f6-41a8-86e4-4c796e8656db","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"546f22c2-02f6-41a8-86e4-4c796e8656db","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"546f22c2-02f6-41a8-86e4-4c796e8656db","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"81a7396f-bc7b-4d67-8036-4d5158c3085b","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"81a7396f-bc7b-4d67-8036-4d5158c3085b","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"81a7396f-bc7b-4d67-8036-4d5158c3085b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"89c333e2-479a-4042-a250-4a5716d84b08","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"89c333e2-479a-4042-a250-4a5716d84b08","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"89c333e2-479a-4042-a250-4a5716d84b08","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"18c3816b-693f-407b-a23b-5b86744212ca","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"18c3816b-693f-407b-a23b-5b86744212ca","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"18c3816b-693f-407b-a23b-5b86744212ca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f2924f77-ec7f-42d3-a922-4a83dd1e6360","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f2924f77-ec7f-42d3-a922-4a83dd1e6360","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"f2924f77-ec7f-42d3-a922-4a83dd1e6360","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c74de07a-4fb5-47cd-9d70-e9ae8d194424","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c74de07a-4fb5-47cd-9d70-e9ae8d194424","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"c74de07a-4fb5-47cd-9d70-e9ae8d194424","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6aa5f79d-49c2-4352-9732-b2c935f56d0b","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6aa5f79d-49c2-4352-9732-b2c935f56d0b","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"6aa5f79d-49c2-4352-9732-b2c935f56d0b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c0e891bb-620f-482a-9245-d9e37871e364","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c0e891bb-620f-482a-9245-d9e37871e364","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"c0e891bb-620f-482a-9245-d9e37871e364","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"de6524bd-5b23-47c6-b661-5599bb6c9980","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"de6524bd-5b23-47c6-b661-5599bb6c9980","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"de6524bd-5b23-47c6-b661-5599bb6c9980","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"adb0e299-36ee-4686-9891-fb584c51202e","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"adb0e299-36ee-4686-9891-fb584c51202e","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"adb0e299-36ee-4686-9891-fb584c51202e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"06729bea-b998-4d1e-b926-e36d74b91e0c","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"06729bea-b998-4d1e-b926-e36d74b91e0c","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"06729bea-b998-4d1e-b926-e36d74b91e0c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ad032a26-5b18-4834-8762-e2385b08453c","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ad032a26-5b18-4834-8762-e2385b08453c","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"ad032a26-5b18-4834-8762-e2385b08453c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fb90dae1-5e29-47b8-b85c-e67808c6486a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fb90dae1-5e29-47b8-b85c-e67808c6486a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"fb90dae1-5e29-47b8-b85c-e67808c6486a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"960e6e0e-f82b-4c2d-bcf1-aea36b3fded8","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"960e6e0e-f82b-4c2d-bcf1-aea36b3fded8","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"960e6e0e-f82b-4c2d-bcf1-aea36b3fded8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"09c577ae-1638-4190-935c-ccaa4abc14b8","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"09c577ae-1638-4190-935c-ccaa4abc14b8","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"09c577ae-1638-4190-935c-ccaa4abc14b8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"613d1fc7-272b-4d13-8281-7d8cc33870fb","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"613d1fc7-272b-4d13-8281-7d8cc33870fb","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"613d1fc7-272b-4d13-8281-7d8cc33870fb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"980add76-e6c5-4603-bb79-07d358d08cf3","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"980add76-e6c5-4603-bb79-07d358d08cf3","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"980add76-e6c5-4603-bb79-07d358d08cf3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dd6b03d2-c66d-4b78-9f58-7a138eb03fb7","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd6b03d2-c66d-4b78-9f58-7a138eb03fb7","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"dd6b03d2-c66d-4b78-9f58-7a138eb03fb7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5dab1544-2fb0-4bf6-8ad1-c26822524504","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5dab1544-2fb0-4bf6-8ad1-c26822524504","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"5dab1544-2fb0-4bf6-8ad1-c26822524504","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fb853f67-8989-4d64-9976-8e521f199ee5","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fb853f67-8989-4d64-9976-8e521f199ee5","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"fb853f67-8989-4d64-9976-8e521f199ee5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"05db91da-7241-487f-b90a-f8043f2fa901","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"05db91da-7241-487f-b90a-f8043f2fa901","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"05db91da-7241-487f-b90a-f8043f2fa901","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4d449970-04d8-40d2-a271-389d756eef48","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4d449970-04d8-40d2-a271-389d756eef48","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"4d449970-04d8-40d2-a271-389d756eef48","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"be37fa62-8f79-4852-9b13-c78340643c48","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"be37fa62-8f79-4852-9b13-c78340643c48","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"be37fa62-8f79-4852-9b13-c78340643c48","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4a00db65-be4b-43c6-83e8-8204da826777","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4a00db65-be4b-43c6-83e8-8204da826777","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"4a00db65-be4b-43c6-83e8-8204da826777","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2b299570-7b34-4f9c-8174-bf549552af5a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2b299570-7b34-4f9c-8174-bf549552af5a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"2b299570-7b34-4f9c-8174-bf549552af5a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"109e51e3-fccd-493a-9e6f-bf6205fb7b65","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"109e51e3-fccd-493a-9e6f-bf6205fb7b65","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"109e51e3-fccd-493a-9e6f-bf6205fb7b65","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e3b141d4-f9a7-4cc0-87f6-baa80726a9c1","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e3b141d4-f9a7-4cc0-87f6-baa80726a9c1","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"e3b141d4-f9a7-4cc0-87f6-baa80726a9c1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"49c61890-1810-432a-af59-52b321ce4768","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"49c61890-1810-432a-af59-52b321ce4768","path":"sprites/spr_green_blood/spr_green_blood.yy",},"LayerId":{"name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","name":"49c61890-1810-432a-af59-52b321ce4768","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 45.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 29.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"fa66d3db-fede-465d-926b-0fd8021f34e5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8c4c7d03-48c1-48df-a0f5-a470c13187ab","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ea840667-6357-4a1c-8f38-0f41f0506e75","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"546f22c2-02f6-41a8-86e4-4c796e8656db","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1dfe13a1-9683-411f-b371-6c8a26f685db","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"81a7396f-bc7b-4d67-8036-4d5158c3085b","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2b1435ff-eaf3-4b0b-b3a2-28fa62474574","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"89c333e2-479a-4042-a250-4a5716d84b08","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dc8f139f-aa7a-4612-bc28-ff8f6695d0e3","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"18c3816b-693f-407b-a23b-5b86744212ca","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f210b450-75a0-4146-a0ae-52301cb007fe","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f2924f77-ec7f-42d3-a922-4a83dd1e6360","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9ef04a76-ecbc-4a44-a33c-584193461e8b","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c74de07a-4fb5-47cd-9d70-e9ae8d194424","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e43e0e76-0d12-4ffc-8430-32f772bef6d9","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6aa5f79d-49c2-4352-9732-b2c935f56d0b","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d8a66e8e-0690-4419-b93b-2caf2891b1e5","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c0e891bb-620f-482a-9245-d9e37871e364","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d8f563ef-625c-4905-93d5-69947b8e5cae","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"de6524bd-5b23-47c6-b661-5599bb6c9980","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c7dbd37a-890d-4ea6-ab3e-22c89e28a1df","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"adb0e299-36ee-4686-9891-fb584c51202e","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"afcd4693-fd73-4d3a-be50-490d173704e1","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"06729bea-b998-4d1e-b926-e36d74b91e0c","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7ba9e678-6113-4c8f-908f-3bb1a48039db","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ad032a26-5b18-4834-8762-e2385b08453c","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7180ba62-9de9-4d51-ae01-3569bd3c394f","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fb90dae1-5e29-47b8-b85c-e67808c6486a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4d4f71b8-90d2-4e54-84b8-b1b984e77d18","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"960e6e0e-f82b-4c2d-bcf1-aea36b3fded8","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"913049d6-f143-4b35-8ede-72dd024aa3ba","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"09c577ae-1638-4190-935c-ccaa4abc14b8","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a7469f80-80e0-4d4c-83b8-e712acb9f8d3","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"613d1fc7-272b-4d13-8281-7d8cc33870fb","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ca9801c1-daf3-442e-92af-9e915d257cae","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"980add76-e6c5-4603-bb79-07d358d08cf3","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"025803b0-3554-4874-8ae6-6866611f5da5","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd6b03d2-c66d-4b78-9f58-7a138eb03fb7","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"90fa9b70-acd6-4009-85a6-89dd6f015fe6","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5dab1544-2fb0-4bf6-8ad1-c26822524504","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9b6a08bc-6252-4afe-930d-2f8b18c099bb","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fb853f67-8989-4d64-9976-8e521f199ee5","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4f0c382e-23e9-4ed9-9cd7-a00f6905fbd9","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"05db91da-7241-487f-b90a-f8043f2fa901","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3135f8b5-cbcb-4c3b-b10b-fc5d4ee85707","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4d449970-04d8-40d2-a271-389d756eef48","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b812c46b-8e52-4189-85fd-1188d1fdc67b","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"be37fa62-8f79-4852-9b13-c78340643c48","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c95cfeff-2e3f-48e0-9528-de92fff2d9cc","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4a00db65-be4b-43c6-83e8-8204da826777","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e3a25db4-4a83-43db-aff0-166b5c6ba7a5","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2b299570-7b34-4f9c-8174-bf549552af5a","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0772ee76-f36a-45b7-b3ee-a17894bde681","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"109e51e3-fccd-493a-9e6f-bf6205fb7b65","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"01dd5127-a852-48e3-af60-b204fa90c854","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e3b141d4-f9a7-4cc0-87f6-baa80726a9c1","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f5caa809-f0a3-4bad-ab0a-3a106703e7a4","Key":28.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"49c61890-1810-432a-af59-52b321ce4768","path":"sprites/spr_green_blood/spr_green_blood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 23,
    "yorigin": 23,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_green_blood","path":"sprites/spr_green_blood/spr_green_blood.yy",},
    "resourceVersion": "1.3",
    "name": "spr_green_blood",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"2403ec6c-d550-4e53-ade8-19d2640b3f7a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "blood",
    "path": "folders/Sprites/blood.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_green_blood",
  "tags": [],
  "resourceType": "GMSprite",
}