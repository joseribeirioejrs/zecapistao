// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_create_life(yPositionDisplacement){
	if (!yPositionDisplacement) {
		yPositionDisplacement = 15
	}
	lifeStatus = instance_create_layer(x, y - yPositionDisplacement, LAYER_INSTANCES, obj_status_life)
	lifeStatus.image_index = 0
	lifeStatus.image_speed = 0
	lifeStatus.image_xscale = .2
	lifeStatus.image_yscale = .2
	lifeStatus.image_alpha = 0
	return lifeStatus
}