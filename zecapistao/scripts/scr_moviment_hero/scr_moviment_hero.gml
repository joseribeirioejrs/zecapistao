// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_moviment_hero(SPEED_IMAGE){
	leftPressed = keyboard_check(vk_left) || keyboard_check(ord("A"))
	rightPressed = keyboard_check(vk_right) || keyboard_check(ord("D"))
	onTheFloor = place_meeting(x, y, obj_colliders_floor)
	onTheLimitMapInit = place_meeting(x, y, obj_colliders_limit_map_init)
	onTheLimitMapEnd = place_meeting(x, y, obj_colliders_limit_map_end)

	if (rightPressed && !onTheLimitMapEnd) {
		image_xscale = 1
		image_speed = SPEED_IMAGE
		x += SPEED_IMAGE
	} else if (leftPressed && !onTheLimitMapInit) {
		image_xscale = -1
		image_speed = SPEED_IMAGE
		x -= SPEED_IMAGE
	} else {
		image_speed = 0
		image_index = 0
	}

	if (!onTheFloor) {
		JUMP += .1
	}
	
	y += JUMP
}