// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_follow_enemy_life_status(lifeStatus){
	if (lifeStatus) {
		lifeStatus.x = x + (sprite_width / 2)
		lifeStatus.y = y - (sprite_height / 2)
	}
}