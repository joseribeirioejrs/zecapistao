isFire = mouse_check_button(mb_left)

if (instance_exists(obj_hero)) {
	image_yscale = obj_hero.image_yscale / 3
	image_xscale = obj_hero.image_xscale / 3
	displacementX = obj_hero.image_xscale > 0 ? 5 : -5
	x = obj_hero.x + displacementX
	y = obj_hero.y - 1
}

if (isFire && !isShooting) {
	instance_create_layer(x, y, "Instances", obj_simple_bullet)
	isShooting = true
	alarm[0] = 1 * room_speed
}