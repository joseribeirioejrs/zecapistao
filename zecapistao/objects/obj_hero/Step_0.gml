scr_moviment_hero(SPEED_IMAGE)

onTheLimitMapInit = place_meeting(x, y, obj_colliders_limit_map_init)
onTheLimitMapEnd = place_meeting(x, y, obj_colliders_limit_map_end)
hasDamage = place_meeting(x, y, obj_enemy_controller)

if ((onTheLimitMapInit || onTheLimitMapEnd) && !hasPlaySound) {
	scr_limit_room()
	hasPlaySound = true
	alarm[0] = 10 * room_speed
} 

if (hasDamage) {
	DEFAULT_IMPACT = 5
	impact_horizontal = DEFAULT_IMPACT
	impact_vertical = DEFAULT_IMPACT
}

if (impact_horizontal > 0) {
	show_debug_message(damageSide)
	if (damageSide == RIGHT_SIDE) {
		x -= impact_horizontal
	} else {
		x += impact_horizontal
	}
	y -= impact_vertical
	impact_horizontal--
	impact_vertical--
}
