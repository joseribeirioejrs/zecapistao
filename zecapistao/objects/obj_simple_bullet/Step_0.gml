wasAgreed = place_meeting(x, y, obj_toast_zombie)

x += SPEED_BULLET * playerPosition

if (wasAgreed) {
	instance_destroy()
}

if (scaleBullet < MAX_SCALE_BULLET) {
	scaleBullet += .01
	image_xscale = X_SCALE_HERO * scaleBullet
	image_yscale = scaleBullet
}

image_alpha -= 0.02

if (image_alpha <= 0) {
	instance_destroy()
}