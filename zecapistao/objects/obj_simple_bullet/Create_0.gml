SPEED_BULLET = 5
MAX_SCALE_BULLET = .5
X_SCALE_HERO = obj_hero.image_xscale
playerPosition = 1
scaleBullet = .1

y += -1

image_yscale = scaleBullet
if (instance_exists(obj_hero)) {
	image_xscale = X_SCALE_HERO * scaleBullet
	x += X_SCALE_HERO > 0 ? 5 : -5
	playerPosition = X_SCALE_HERO
}