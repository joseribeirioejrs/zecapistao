if (enemiesAmount > 0) {
	leftOrRightPosition = choose(START_POSITION, WIDTH_ROOM - 15)
	enemyCreated = instance_create_layer(leftOrRightPosition, Y_POSITION, LAYER_INSTANCES, obj_toast_zombie)
	if (leftOrRightPosition == START_POSITION) {
		enemyCreated.speedZombie *= -1 
		enemyCreated.image_xscale *= -1
	}
	timeToCreateEnemy = choose(1, 2, 3, 4, 5)
	alarm[0] = room_speed * timeToCreateEnemy
	enemiesAmount--
	show_debug_message("Resta " + string(enemiesAmount))
}
