
onTheFloor = place_meeting(x, y, obj_colliders_floor)
wasAgreed = place_meeting(x, y, obj_simple_bullet)
hasDamage = place_meeting(x, y, obj_hero)
hasBuild = place_meeting(x, y, obj_build_controller)

x -= speedZombie

// scr_follow_enemy_life_status(lifeStatus)

if (!onTheFloor) {
	verticalSpeedZombie += .2
} else {
	verticalSpeedZombie = 0
}

y += verticalSpeedZombie

if (wasAgreed) {
	instance_create_layer(x, y, LAYER_INSTANCES, obj_blood)
}

if (wasAgreed && life > 0) {
	life--
}
 
if (wasAgreed && life <= 0) {
	// lifeStatus.image_alpha = 0
	sprite_index = spr_toast_zombie_die
	speedZombie = 0
}

if (life <= 0 && image_index > image_number - 1 && sprite_index == spr_toast_zombie_die) {
	sprite_index = spr_toast_zombie_ghost
	image_index = 0
	y -= 14
}

if (life <= 0 && image_index > image_number - 1 && sprite_index == spr_toast_zombie_ghost) {
	instance_destroy()
}

if (hasDamage && instance_exists(obj_hero)) {
	obj_hero.damageSide = speedZombie < 0 ? obj_hero.RIGHT_SIDE : obj_hero.LEFT_SIDE
}

speedZombie = hasBuild ? 0 : DEFAULT_SPEED_ZOMBIE
