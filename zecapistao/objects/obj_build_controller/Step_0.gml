hasToastEnemy = place_meeting(x, y, obj_toast_zombie)

if (hasToastEnemy && !hasDamageToastEnemy) {
	show_debug_message("Era para eu tomar dano")
	hasDamageToastEnemy = true
	alarm[ALARM_TOAST_ENEMY_DAMAGE] = room_speed
} else if (!hasToastEnemy) {
	hasDamageToastEnemy = false
}

if (life <= 0) {
	instance_destroy(lifeStatus)
	instance_destroy()
}