life--
lifeStatus.image_alpha = 1

lifeRemaining = life / INITIAL_LIFE
imageIndexLifeStatus = 0

if (lifeRemaining > .75) {
	imageIndexLifeStatus = 0
} else if (lifeRemaining > .5) {
	imageIndexLifeStatus = 1
} else if (lifeRemaining > .25) {
	imageIndexLifeStatus = 2
} else {
	imageIndexLifeStatus = 3
}

lifeStatus.image_index = imageIndexLifeStatus

if (hasToastEnemy) {
	alarm[ALARM_TOAST_ENEMY_DAMAGE] = room_speed
}
